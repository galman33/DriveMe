(function() {
'use strict';

    angular
        .module('app')
        .controller('DriversSearch', DriversSearch);

    DriversSearch.$inject = ['friendsList'];
    function DriversSearch(friendsList) {
        var vm = this;
        
        vm.driversDictionary = {};
        vm.searchDrivers = searchDrivers;
        vm.nameFilter = '';

        activate();

        //////////////// 

        function activate() {
            searchDrivers()
        }
        
        function searchDrivers() {
            vm.driversDictionary = getDriversDictionary()
        }
        
        function getDriversDictionary() {
            var driversDictionary = []
            var chars = getChars()
            for(let char of chars)
                driversDictionary.push({char: char, drivers: getDriversByChar(char)})
            
            return driversDictionary
        }
        
        function getChars() {
            var friends = getFilteredFriends()
            
            var friendsNames = friends.map((friend) => friend.name)
            friendsNames.sort()
            
            var chars = []
            var prevChar = ''
            
            for(let friendName of friendsNames)
            {
                if(friendName[0].toUpperCase() != prevChar)
                {
                    chars.push(friendName[0].toUpperCase())
                    prevChar = friendName[0].toUpperCase()
                }
            }
            
            return chars;
        }
        
        function getDriversByChar(char) {
            var friends = getFilteredFriends()
            
            var drivers = []
            
            for(let friend of friends)
                if(friend.name[0].toUpperCase() == char)
                    drivers.push(friend)
                    
            return drivers
        }
        
        function getFilteredFriends() {
            var filteredFreinds = []
            for(let friend of friendsList.getFriends())
                if(friend.name.toLowerCase().indexOf(vm.nameFilter.toLowerCase()) > -1)
                    filteredFreinds.push(friend)
            return filteredFreinds
        }
    }
})();